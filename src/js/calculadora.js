
class InputRange{
    constructor(parentDiv,moneyFormat = false){
        this.parentDiv = parentDiv;
        this.moneyOutput = moneyFormat; 
        this.input = this.getInputChild();
        this.output = this.getOutputChild();
        this.input.addEventListener("input",this.setOutputValue.bind(this));
        this.id = parentDiv.id; 
        // una nueva hoha de estilo
        this.nuevaHojaDeEstilo = document.createElement("style");
        document.head.appendChild(this.nuevaHojaDeEstilo);
    }
    getValue(){
        return this.input.value; 
    }
    CSSstyle(){
        let min = this.input.min; 
        let max = this.input.max;  
        let currentValue = this.input.value; 

        let interval = max-min;
        let breakPoint = (currentValue - min)/interval*100;
        let style = `
        #${this.id} input[type=range]::-webkit-slider-runnable-track{ 
          background-image:-webkit-linear-gradient(left, #6CA358 ${breakPoint}%, black ${breakPoint}%)
        }
          #${this.id} input[type=range]::-moz-range-track{ 
            background-image:-moz-linear-gradient(left, #6CA358 ${breakPoint}%, black ${breakPoint}%)
            }
          `;
          this.nuevaHojaDeEstilo.textContent = style;
    }
    getInputChild(){
        return this.parentDiv.querySelector('input'); 
    }
    getOutputChild(){
        return this.parentDiv.querySelector('.output'); 
    }
    setRangeControl(min,max){
        this.input.setAttribute('min',min);
        this.input.setAttribute('max',max);
        let value = (min+max)/2;
        this.input.setAttribute('value',value);
        this.setOutputValue();
    }
    setOutputValue(){
        this.CSSstyle();
        let outputValue = this.input.value;
        let event = new Event('calculate');
        this.input.dispatchEvent(event);
        if(this.moneyOutput){
            this.output.textContent = this.moneyFormat(outputValue);
        }
        else{
            this.output.textContent = outputValue;
        }
    }
    moneyFormat(number){
        number = number.toString();
        let lenNumber = number.length;
        let format = '$';
        while(lenNumber>3){
            let value = Number(number);
            let parteEntera = Math.floor(value/1000);
            let lenPartEntera = parteEntera.toString().length;
            number = number.substring(lenPartEntera);
            lenNumber = number.length;
            format += parteEntera.toString()+',';
        }
        format += number+'.°°';
        return format; 
    }
    toMoney(){
        let moneyf = this.moneyFormat(this.output.textContent);
        this.output.textContent = moneyf;
    }
  }






let divIngreso = document.getElementById('Ingreso');
let divGastoAlimento = document.getElementById('Gasto-alimento');
let divGastoTransporte = document.getElementById('Gasto-transporte');
let divGastoServicio = document.getElementById('Gasto-servicio');
let divGananciaMensual = document.getElementById('Ganancia-mensual');
let divEdad = document.getElementById('Edad');


let ingreso = new InputRange(divIngreso,true);
let gastoAlimento = new InputRange(divGastoAlimento,true);
let gastoTransporte = new InputRange(divGastoTransporte,true);
let gastoServicio = new InputRange(divGastoServicio,true);
let gananciaMensual = new InputRange(divGananciaMensual,true);
let edad = new InputRange(divEdad);

ingreso.setRangeControl(3000,60000);
gastoAlimento.setRangeControl(1000,25000);
gastoTransporte.setRangeControl(0,15000);
gastoServicio.setRangeControl(0,20000);
gananciaMensual.setRangeControl(0,50000);
edad.setRangeControl(18,99);

function calculateSavings(){
    let income = Number(ingreso.getValue());
    let alimentos= Number(gastoAlimento.getValue());
    let servicios = Number(gastoServicio.getValue());
    let transporte= Number(gastoTransporte.getValue());
    let ganancia = Number(gananciaMensual.getValue());
    
    let incomes = income+ganancia;
    let outcomes = alimentos+servicios+transporte;
    let savings = incomes - outcomes;
    if (savings<=0){
        savings = 0; 
    }
    let outputSavings = document.querySelector('.boxy h4');
    savings = ingreso.moneyFormat(savings);
    outputSavings.textContent = savings; 
    console.log("hi");
}

calculateSavings();
ingreso.getInputChild().addEventListener('calculate',calculateSavings);
gastoAlimento.getInputChild().addEventListener('calculate',calculateSavings);
gastoTransporte.getInputChild().addEventListener('calculate',calculateSavings);
gastoServicio.getInputChild().addEventListener('calculate',calculateSavings);
gananciaMensual.getInputChild().addEventListener('calculate',calculateSavings);
edad.getInputChild().addEventListener('calculate',calculateSavings);